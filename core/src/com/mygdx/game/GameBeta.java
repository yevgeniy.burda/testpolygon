package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mygdx.game.Entities.ActorBeta;

public abstract class GameBeta extends Game {

    protected SpriteBatch batch;
    protected Stage mainStage;


    @Override
    public void create()
    {
        batch = new SpriteBatch();
        mainStage = new Stage();
        initialize();
    }


    @Override
    public void render() {
        float dt = Gdx.graphics.getDeltaTime();
        mainStage.act(1 / 60f);
        update(dt);
        batch.begin();
        mainStage.draw();
        batch.end();
    }

    @Override
    public void dispose() {
        batch.dispose();
//        for (ActorBeta tempHero : heroList) {
//          //  tempHero.getHeroSkin().dispose();
//        }
//        hero.getHeroSkin().dispose();
//        //enemy.getHeroSkin().dispose();
//        img.dispose();
    }


    // create method remains the same
    public abstract void initialize();
    // render method remains the same
    public abstract void update(float dt);
}
