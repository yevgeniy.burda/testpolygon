package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ActorBeta extends Actor {
    private TextureRegion textureRegion;
    private Rectangle rectangle;
    private int target = 0;
    private int rotationDegree;

    public ActorBeta(int target) {
        super();
        textureRegion = new TextureRegion();
        rectangle = new Rectangle();
        this.target = target;
    }

    public void setTexture(Texture t) {
        textureRegion.setRegion(t);
        setSize(t.getWidth(), t.getHeight());
        rectangle.setSize(t.getWidth(), t.getHeight());
    }

    public Rectangle getRectangle() {
        rectangle.setPosition(getX(), getY());
        return rectangle;
    }

    public boolean overlaps(ActorBeta other) {
        return this.getRectangle().overlaps(other.getRectangle());
    }

    public void act(float dt) {
        super.act(dt);
        if (Gdx.input.isTouched()) {
            this.moveBy(0, (float) -1 * this.target);
            this.setRotation(rotationDegree++);
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        Color c = getColor(); // used to apply tint color effect
        batch.setColor(c.r, c.g, c.b, c.a);
        Gdx.app.log("visible: ", String.valueOf(isVisible()));
        Gdx.app.log("getX(): ", String.valueOf(getX()));
        Gdx.app.log("getY(): ", String.valueOf(getY()));
        Gdx.app.log("getOriginX(): ", String.valueOf(getOriginX()));
        Gdx.app.log("getOriginY(): ", String.valueOf(getOriginY()));
        Gdx.app.log("getScaleX(): ", String.valueOf(getScaleX()));
        Gdx.app.log("getScaleY(): ", String.valueOf(getScaleY()));
        Gdx.app.log("getRotation(): ", String.valueOf(getRotation()));
        if (isVisible())
            batch.draw(textureRegion,
                    getX(), getY(), getOriginX(), getOriginY(),
                    getWidth()*2, getHeight()*2, getScaleX(), getScaleY(), getRotation()+30);
    }
}